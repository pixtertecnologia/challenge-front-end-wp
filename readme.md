# Teste de desenvolvimento Front End

[![N|Solid](https://pixter.com.br/wp-content/uploads/2018/06/logo-black.png)](http://pixter.com.br)

Layout disponível em (https://www.figma.com/file/6gf0UxEqNYEPKPVXFoJPP1/Challenge-Front-End)

A proposta do teste é:

- Criar um projeto em **WordPress**, que contemple o cadastro de livros (campos conforme o layout)
- Criar um tema _responsivo_ do projeto fictício **_Pixter Books_**
- Na seção **Books** da _home_ trazer os **8 últimos** livros cadastrados
- Ao clicar num livro, ir até a página de detalhe do mesmo
- Em **Newsletter**, cadastrar o email do usuário que receberá atualizações

### Pontuaremos:

- Estrutura de projeto
- Genericidade de código (componentes reutilizáveis)
- Patterns
- Organização de CSS
- Responsividade

### Requisitos:

- Desenvolver em PHP na versão 5.6+

### Entrega:

- Faça um _**fork**_ deste repositório
- Desenvolva sua aplicação
- Envie as atualizações para seu repositório
- Faça um _**pull request**_
- Envie-nos um e-mail confirmando a finalização
